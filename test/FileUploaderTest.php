<?php

use PHPUnit\Framework\TestCase;
use Attic\Files\FileUploader;

class FileUploaderTest extends TestCase
{
    public function testDelete()
    {
        $uploader = new FileUploader();
        $file_name = './nothing-here.pdf';

        $this->assertFalse($uploader->delete($file_name));

        touch($file_name);

        $this->assertTrue($uploader->delete($file_name));
        $this->assertFalse(is_file($file_name));
    }

    public function testIsValidExtension()
    {
        $uploader = new FileUploader();
        $valid_extensions = array('png', 'doc');

        $this->assertTrue($uploader->isValidExtension('png', $valid_extensions));
        $this->assertFalse($uploader->isValidExtension('jpg', $valid_extensions));
    }

    public function testSanitizeFileName()
    {
        $uploader = new FileUploader();
        $file_name = '../$cär¥';

        $this->assertEquals('..cr', $uploader->sanitizeFileName($file_name));
    }

    public function testRearrangeArray()
    {
        $uploader = new FileUploader();
        $originalArray = array('name' => array('one', 'two'), 'other' => array('three', 'four'));
        $fixedArray = array(array('name' => 'one', 'other' => 'three'), array('name' => 'two', 'other' => 'four'));

        $this->assertEquals($fixedArray, $uploader->rearrangeArray($originalArray));
    }
}