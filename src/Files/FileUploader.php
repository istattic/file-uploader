<?php

namespace Attic\Files;

class FileUploader
{

    /**
     * Performs security checks on newly uploaded file(s) and moves them to the specifed storage location
     * 
     * @param array  $uploaded_files $_FILES array
     * @param string $storage_path   absolute path to the location where the file(s) should be stored
     * 
     * @return array success value and an array of the files with their attributes
     */
    public function store($uploaded_files, $storage_path, $allowed_extensions) 
    {
        $files = $this->rearrangeArray($uploaded_files);
        $success = true;

        foreach ($files as &$file) {
            $base_name = $this->sanitizeFileName(pathinfo($file['name'], PATHINFO_FILENAME));
            $hash = md5(date('Y-m-d H:m:s') . $file['name']);
            $extension = pathinfo($file['name'], PATHINFO_EXTENSION);

            if (!$this->isValidExtension($extension, $allowed_extensions)) {
                $file['error'] = 'Invalid file type';
                $success = false;
                continue;
            }

            $physical_file_name = "$base_name-$hash.$extension";
            $file['physical_file_path'] = "$storage_path/$physical_file_name";

            if (!$file['size'] > 0 || !move_uploaded_file($file['tmp_name'], $file['physical_file_path'])) {
                $file['error'] = 'There was a problem saving the file to the system';
                $success = false;
                continue;
            }
        }

        return array('success' => $success, 'files' => $files);
    }

    /**
     * Deletes a file from the file system
     * 
     * @param string $file_path Absolute path of the file to be deleted
     * 
     * @return boolean true on success, false on failure
     */
    public function delete($file_path) 
    {
        if (is_file($file_path)) {
            if (unlink($file_path)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Validates that a file extension is valid according to the specified list
     * 
     * @param string $extension        The file extension to be validated, without leading period
     * @param array  $valid_extensions The list of allowed extensions
     * 
     * @return boolean true if the extension is valid, false if it is not
     */
    public function isValidExtension($extension, $valid_extensions) 
    {
        $extension = strtolower($extension);

        if (in_array($extension, $valid_extensions)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sanitizes file names by removing any characters that are not alphanumeric, dashes, underscores, or periods, and truncating it to 225 characters
     * 
     * @param string $filename The file name to be sanitized
     * 
     * @return string The sanitized file name
     */
    public function sanitizeFileName($filename) 
    {
        $sanitized_name = preg_replace('/[^-0-9A-Za-z_\.]/', '', $filename);
        $short_name = substr($sanitized_name, 0, 225);
        return $short_name;
    }

    /**
     * Rearranges the $_FILES array so it's easier to use
     * 
     * @param array $_FILES array
     * 
     * @return array The rearranged array
     */
    public function rearrangeArray($file_post) 
    {
        $files = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i = 0; $i < $file_count; $i++) {
            foreach ($file_keys as $key) {
                $files[$i][$key] = $file_post[$key][$i];
            }
        }

        return $files;
    }
}

?>