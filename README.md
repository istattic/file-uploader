# File Uploader #
Processes files uploaded through a form safely and provides a way to delete them.

## Run Tests ##

```
#!php

phpunit test --bootstrap vendor/autoload.php

```